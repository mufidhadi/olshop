<?php
defined('BASEPATH') or exit('No direct script access allowed');

class data_login extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }

  function insert_member($post)
  {
    $cek_email = $this->db->get_where('tb_user', array('email' => $post['email']))->num_rows();
    if ($cek_email > 0)
    {
      $msg = bin2hex('Maaf, email sudah terdaftar, gunakan email lain untuk mendaftar');
      redirect(base_url('signup').'?status=500&msg='.$msg);
    } else {
      $data = array(
        'username' 			=> $post['username'] ,
        'hp'		=> $post['hp'],
        'alamat'		=> $post['alamat'],
        'email' 		=> $post['email'],
        'password' 		=> $this->encryption->encrypt($post['password']),
        'sumber'		=> 'email',
        'hak_akses'		=> 'member',
      );
      $this->db->insert('tb_user',$data);
      return $data;
    }
  }


  function getUser($token)
  {
    // ambil informasi profil user
  }

  function getEmail($access_token)
  {
    // ambil data email
  }

}
