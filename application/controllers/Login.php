<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct(){
		parent::__construct();
		//komponen google client library
		$this->load->model('m_TokoOnline');
		$this->load->model('data_login');
	}

	public function index()
	{
		$this->load->view('login');
	}

	public function sign_up()
	{
		$this->load->view('sign_up');
	}

	public function insert()
	{
		$post = $this->input->post();
		$this->session->set_userdata($post);
		$this->data_login->insert_member($post);
		redirect(base_url('login'));
	}

	function aksi_login()
	{
		$email = $_POST['email'];
		$password = $_POST['password'];
		$user = $this->db->get_where('tb_user', array('email' => $email,'hak_akses' => 'member'))->row_array();
		$enpass=$this->encryption->decrypt($user['password']);
		if ($user) {
			if ($this->encryption->decrypt($user['password']) == $password){
				$data_session = array(
					'id_user' => $user['id_user'],
					'email' => $user['email'],
					'hak_akses' => $user['hak_akses'],
				);
				$this->session->set_userdata($data_session);
				switch ($user['hak_akses']) {
					case 'admin':
					$url = base_url('admin/home');
					break;
					case 'member':
					$url = base_url('home');
					break;
					default:
					# code...
					break;
				}
				redirect($url);
			}else {
				redirect(base_url('login'));
			}
		}
	}

	function login_g()
	{
		//login google
	}

	public function logout(){
		session_destroy();
		redirect();
	}

}
