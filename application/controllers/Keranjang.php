<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keranjang extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_TokoOnline');
	}

	public function index()
	{
		cek_auth();
		$data['email'] = $_SESSION['email'];
		$this->load->view('keranjang');
	}

	function remove_item(){
		$rowid= $this->input->post('rowid');
		$this->cart->remove($rowid);
	}

	function clear_keranjang(){
		$this->cart->destroy();
		redirect();
	}

	function checkout(){
		if($this->cart->total_items()<=0){echo 'Keranjang Kosong';}
		else{
			$no_transaksi = $this->m_TokoOnline->autonumber();
			$data = array(
				'no_transaksi'		=> $no_transaksi,
				'id_user'		=> $_SESSION['id_user'],
				'jumlah_item'	=> $this->cart->total_items(),
				'total_bayar'	=> $this->cart->total(),
				'tanggal'	=> date("Y/m/d"),
				'status'	=> 'Pending'
			);
			$this->m_TokoOnline->insert_transaksi($data);
			foreach ($this->cart->contents() as $item):
				$data = array(
					'no_detail'		=> '',
					'no_transaksi'		=> $no_transaksi,
					'id_barang'	=> $item['id'],
					'jumlah'	=> $item['qty']
				);
				$this->m_TokoOnline->insert_detail_transaksi($data);
			endforeach;
			$this->cart->destroy();
		}

	}


}
