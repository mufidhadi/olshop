<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Produk Detail</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Bootstrap glyphicon CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.css');?>" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('assets/css/portfolio-item.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/toastr.min.css');?>" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="<?php echo base_url('home'); ?>">OlShopKu</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <!-- <input type="text" class="form-control input-sm" placeholder="Cari"> -->
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Beranda
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('keranjang'); ?>"><span class="glyphicon glyphicon-shopping-cart"></span>  <?php echo $this->cart->total_items(); ?> Items</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#"><?php echo @$email;?></a>  </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('login/logout'); ?>">Logout</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">

      <!-- Portfolio Item Heading -->
      <h1 class="my-5"><?php echo $nama; ?>
        <small><?php echo $nama ?></small>
      </h1>

      <!-- Portfolio Item Row -->
      <div class="row">

        <div class="col-md-8">
          <img class="img-fluid" src="<?php echo $gambar ?>" alt="">
        </div>
        <div class="col-md-4">
          <small class="text-muted" style="font-size:16px;">Harga</small>
            <h3 class="my-0" style="color:#d71149;" >Rp<?php echo number_format($harga,0,",",".");  ?></h3>

          <h3 style="margin-top:20px; ">Deskripsi</h3>
          <p class="h-50 d-inline-block"><?php echo $deskripsi ?></p>
          <h3 class="my-3"></h3>
          <button type="button" onclick="tambah('<?php echo $id_barang ?>','<?php echo $nama ?>','<?php echo $harga ?>');" class="btn btn-info btn-block">Beli</button>

        </div>

      </div>
      <!-- /.row -->

      <!-- Related Projects Row -->
      <h3 class="my-4">Related Projects</h3>

      <div class="row">

        <div class="col-md-3 col-sm-6 mb-4">
          <a href="#">
            <img class="img-fluid" src="http://placehold.it/500x300" alt="">
          </a>
        </div>

        <div class="col-md-3 col-sm-6 mb-4">
          <a href="#">
            <img class="img-fluid" src="http://placehold.it/500x300" alt="">
          </a>
        </div>

        <div class="col-md-3 col-sm-6 mb-4">
          <a href="#">
            <img class="img-fluid" src="http://placehold.it/500x300" alt="">
          </a>
        </div>

        <div class="col-md-3 col-sm-6 mb-4">
          <a href="#">
            <img class="img-fluid" src="http://placehold.it/500x300" alt="">
          </a>
        </div>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Your Website 2018</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- core JavaScript -->
    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.bundle.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/toastr.js');?>"></script>

    <script>
    // TAMBAH ITEM KE KERANJANG
    function tambah(id_barang,nama,harga){
      $.ajax({
        url: '<?php echo base_url('produk/tambah_keranjang')?>',
        type: 'POST',
        data: {id_barang: id_barang,nama:nama,harga:harga}
      })
      .done(function() {
        console.log("success");
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
        toastr.success('telah ditambahkan dalam keranjang', 'Berhasil!')
        // location.reload();
        setTimeout(function(){ location.reload(); }, 1000);
      });
    }
    </script>

    <!-- koding chat telegram -->

  </body>

</html>
