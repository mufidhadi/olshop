<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Admin OlShopku</title>

  <link href="<?php echo base_url('/vendor/bootstrap/css/bootstrap.css') ?>" rel='stylesheet' type='text/css' />
  <link href="<?php echo base_url('/vendor/bootstrap/css/bootstrap.min.css') ?>" rel='stylesheet' type='text/css' />
  <link href="<?php echo base_url('/vendor/bootstrap/css/font-awesome.css') ?>" rel='stylesheet' type='text/css' />
  <link href="<?php echo base_url('/vendor/bootstrap/css/sb-admin.css') ?>" rel='stylesheet' type='text/css' />
  <link href="<?php echo base_url('/assets/css/sweetalert.css') ?>" rel='stylesheet' type='text/css' />
  <link href="<?php echo base_url('/vendor/bootstrap/js/dataTables.bootstrap4.css') ?>" rel='stylesheet' type='text/css' />

</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

  <!-- Modal -->
<div class="modal fade" id="modal_detail_pesanan" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Detail Pesanan</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

  <!-- Navigation-->
  <?php $this->load->view('template/navbar.php'); ?>
  <!-- Navigation-->

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">My Dashboard</li>
      </ol>
      <!-- Icon Cards-->
      <div class="row">
        <div class="col-xl-3 col-sm-6 mb-3">
          <div class="card text-white bg-primary o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-users"></i>
              </div>
              <div class="mr-5"><?php echo $pengguna; ?> Members!</div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="<?php echo base_url('pengguna') ?>">
              <span class="float-left">View Details</span>
              <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
            </a>
          </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-3">
          <div class="card text-white bg-danger o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-list"></i>
              </div>
              <div class="mr-5"><?php echo $item; ?> Items!</div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="<?php echo base_url('admin_produk') ?>">
              <span class="float-left">View Details</span>
              <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
            </a>
          </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-3">
          <div class="card text-white bg-success o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-shopping-cart"></i>
              </div>
              <div class="mr-5"><?php echo count($transaksi); ?> New Orders!</div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="<?php echo base_url('admin_pesanan') ?>">
              <span class="float-left">View Details</span>
              <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
            </a>
          </div>
        </div>

      </div>
      <!-- Area Chart Example-->


      <!-- Example DataTables Card-->

    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © Your Website 2018</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Apakah anda yakin?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Pilih "Logout" untuk keluar.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->

    <script src="<?php echo base_url('/assets/js/jquery.min.js') ?>"></script>

    <script src="<?php echo base_url('/assets/js/bootstrap.bundle.min.js') ?>"></script>
    <script src="<?php echo base_url('/assets/js/jquery.dataTables.js') ?>"></script>
    <script src="<?php echo base_url('/assets/js/dataTables.bootstrap4.js') ?>"></script>
    <script src="<?php echo base_url('/assets/js/sb-admin.min.js') ?>"></script>
    <script src="<?php echo base_url('/assets/js/sb-admin-datatables.min.js') ?>"></script>
    <script src="<?php echo base_url('/assets/js/sweetalert.min.js') ?>"></script>

  </div>
</body>

</html>
