<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Admin OlShopku</title>

  <!-- Bootstrap core CSS-->
  <?php $this->load->view('template/css.php'); ?>

</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

  <!-- Navigation-->
  <?php $this->load->view('template/navbar.php'); ?>
  <!-- Navigation-->

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Pengguna</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Data Pengguna</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr align='center'>
                  <th>ID User</th>
                  <th>Email</th>
                  <th>Username</th>
                  <th>Hak Akses</th>
                  <th>Alamat</th>
                  <th>No. HP</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>ID User</th>
                  <th>Email</th>
                  <th>Username</th>
                  <th>Hak Akses</th>
                  <th>Alamat</th>
                  <th>No. HP</th>
                </tr>
              </tfoot>
              <tbody>
                <?php foreach ($item as $data_item): ?>
                <tr>
                  <td><?php echo $data_item->id_user ?></td>
                  <td align='center'><?php echo $data_item->email ?></td>
                  <td align='center'><?php echo $data_item->username ?></td>
                  <td align='center'><?php echo $data_item->hak_akses ?></td>
                  <td align='center'><?php echo $data_item->alamat ?></td>
                  <td align='center'><?php echo $data_item->hp ?></td>
                  <td align='center' class="none">
                    <button type="button" class="btn btn-warning" onclick="tampilUbahData('<?php echo $data_item->id_user ?>')"><span class="glyphicon glyphicon-pencil"></span></button>
                    <button type="button" class="btn btn-danger" onclick="hapusData('<?php echo $data_item->id_user ?>')"><span class="glyphicon glyphicon-trash"></span></button>
                  </td>

                </tr>

                <?php endforeach; ?>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © Your Website 2018</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Apakah anda yakin?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Pilih "Logout" untuk keluar.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>

                                            <!--Ubah Modal -->

    <div class="modal fade" id="ubahModal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title"> <b>Data Pengguna </b></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">

            <form action="admin/Pengguna/ubahData" method="post" id="form_ubah_pengguna" enctype="multipart/form-data">
              <input type="text" class="form-control" id="id_user" name="id_user" required oninvalid="this.setCustomValidity('data tidak boleh kosong')" hidden>

              <div class="form-group">
                <label for="recipient-name" class="control-label">Username:</label>
                <input type="text" class="form-control" id="username" name="username" required oninvalid="this.setCustomValidity('data tidak boleh kosong')" oninput="setCustomValidity('')">
              </div>

              <div class="form-group">
                <label for="recipient-name" class="control-label">Email:</label>
                <input type="text"   class="form-control" id="email" name="email" required oninvalid="this.setCustomValidity('data tidak boleh kosong')" oninput="setCustomValidity('')">
              </div>

              <div class="form-group">
                <label for="recipient-name" class="control-label">Alamat:</label>
                <textarea rows='3' class="form-control" id="alamat" name="alamat" required oninvalid="this.setCustomValidity('data tidak boleh kosong')" oninput="setCustomValidity('')"></textarea>
              </div>
              <div class="form-group">
                <label for="recipient-name" class="control-label">No. HP:</label>
                <input type="text" class="form-control" id="no_hp" name="no_hp" required oninvalid="this.setCustomValidity('data tidak boleh kosong')" oninput="setCustomValidity('')">
              </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            <button type="submit" onclick="" class="btn btn-primary">Simpan</button>
          </div>
        </div>
      </div>
    </div>
    </form>

          <!-- Modal -->

    <!-- Bootstrap core JavaScript-->
    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.bundle.min.js');?>"></script>
    <!-- Core plugin JavaScript-->
    <script src="<?php echo base_url('assets/js/jquery.easing.min.js');?>"></script>
    <!-- Page level plugin JavaScript-->
    <script src="<?php echo base_url('assets/js/jquery.dataTables.js');?>"></script>
    <script src="<?php echo base_url('assets/js/dataTables.bootstrap4.js');?>"></script>
    <!-- Custom scripts for all pages-->
    <script src="<?php echo base_url('assets/js/sb-admin.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/sweetalert.min.js');?>"></script>
    <!-- Custom scripts for this page-->
    <script src="<?php echo base_url('assets/js/sb-admin-datatables.min.js');?>"></script>
  </div>
</body>

</html>


<script>
function tampilUbahData(id_user){

  $.ajax({
    url : '<?php echo base_url('admin/Pengguna/tampilUbahData')?>',
    type: 'post',
    data: {id_user: id_user},

    success: function(hasil) {
      $response = $(hasil);
      // // ambil data dari url admin/edit_barang?id_barang=(sekian)
      var id_user = $response.filter('#id_user').text();
      var username = $response.filter('#username').text();
      var email = $response.filter('#email').text();
      var alamat = $response.filter('#alamat').text();
      var no_hp = $response.filter('#no_hp').text();
      // menampilkan ke modal
      $('#id_user').val(id_user);
      $('#username').val(username);
      $('#email').val(email);
      $('#alamat').val(alamat);
      $('#no_hp').val(no_hp);
      $('#ubahModal').modal('show');
      // alert(deskripsi);
      // console.log($response);

    }
  });
  }



  function hapusData(id_user){

    swal({
  title: "Apakah anda yakin?",
  text: "Data akan hilang secara permanen!",
  type: "warning",
  showCancelButton: true,
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Ya, hapus!",
  closeOnConfirm: false
},
function(){
  swal("Terhapus!", "Data telah dihapus.", "success");

  $.ajax({
    url : '<?php echo base_url('admin/Pengguna/hapusData')?>',
    type: 'post',
    data: {id_user: id_user},

    success: function(hasil) {
      setTimeout(function(){ location.reload(); }, 1000);
    }
  });


});


    }

  // === End Function Hapus Produk =======


  </script>
