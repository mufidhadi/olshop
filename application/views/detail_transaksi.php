<div>
<table class="table table-condensed">
  <thead>
    <tr>
      <th>No</th>
      <th>Nama Item</th>
      <th>Harga</th>
      <th>Jumlah</th>
      <th>Total Bayar</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $no=0;
    foreach ($detail as $item):
     $no+=1;
      ?>
    <tr>
      <td><?php echo $no ?></td>
      <td><?php echo $item->nama ?></td>
      <td>Rp.<?php echo number_format($item->harga,0,",","."); ?></td>
      <td align='center'><?php echo $item->jumlah ?></td>
      <td>Rp.<?php echo number_format($item->harga*$item->jumlah,0,",","."); ?></td>
    </tr>

  <?php endforeach; ?>

  </tbody>
</table>
</div>
